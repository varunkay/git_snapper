import json
from os import path
from configparser import ConfigParser
from logging_settings import logging_settings
from advert_snapper.advert_snapper import AdvertSnapper
from database_accessor.database_accessor import DBAccessor
from file import File
from model import Model
from utility.dictionary import get_nested
from webdriver import browser


class Snapper(object):
    """
    Main Object..
    """

    def __init__(self):
        """ Initialise instance """

        self.dir_path = path.abspath(path.join('__file__', ".."))

        # snapper_settings config init
        self.cfg = ConfigParser()
        self.cfg.read('{dir_path}/config/snapper_settings.ini'.format(dir_path=self.dir_path))

        # # Setup logging settings
        # self.log = logging_settings()

        # Initialise objects
        self.query = Model()
        self.advert_snapper = AdvertSnapper()
        self.database_accessor = DBAccessor()
        self.file = File()

        # chrome driver location
        self.web_driver = '{dir_path}/chromedriver'.format(dir_path=self.file.file_path_levels("../.."))


if __name__ == "__main__":

    snapper = Snapper()

    # Checks for log/store directory exists, if not creates it
    snapper.file.check_log_store_dir()

    # snapper.log.debug('Snapper Initiated...')

    # Sleep time intervals
    snapper_settings = snapper.query.select_from_snapper_settings()

    # [3, 5, 15]
    sleep_time = json.loads(get_nested(snapper_settings[0], 'dict_value')).get('snapshot_interval')[0]

    while True:

        try:
            # Select query to retrieve all unprocessed adverts (processed=0)
            unprocessed_adverts = snapper.query.select_unprocessed_ads()

            # count unprocessed adverts
            count = len(unprocessed_adverts)

            # List of processed adverts (unused, for reference)
            pipeline = []

            if count > 0:
                # index for loop reference
                index = 0
                for adverts in unprocessed_adverts:
                    index += 1

                    # Make directory and return file path of dir
                    file_path = snapper.file.create_image_store_dir(adverts['country'], adverts['ad_uid'])

                    # Web driver
                    driver = browser(file_path=file_path, web_driver=snapper.web_driver)

                    # Fetches image, makes dir, process image and returns dict for updating database
                    advert_dict = snapper.advert_snapper.fetch_image(adverts=adverts, interval=sleep_time, count=count,
                                                                     index=index, file_path=file_path, driver=driver)

                    # Updates database
                    snapper.database_accessor.execute_query(adverts=adverts, advert_dict=advert_dict)

                    # List of all advert dicts that was processed...
                    pipeline.append(advert_dict)

            # To reduce load on DB Server
            # else:
            #     time.sleep(5)
            #     print("sleeping...")

        except IndexError:
            snapper.log.error('1')
