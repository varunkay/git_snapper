import os
from os import path
import re
from datetime import datetime
from PIL import Image, ImageChops
import pytesseract
from logging_settings import logging_settings


# /Users/varunkapadia/git_workspace/snapper2/src
DIR_PATH = path.abspath(path.join('__file__', ".."))


class File(object):

    def __init__(self):

        self.log = logging_settings()

    def file_path_levels(self, level):
        return path.abspath(path.join('__file__', level))

    def check_dir_exists(self, path):
        if os.path.isdir(path):
            self.log.debug("Advert has already been processed, dir: {dir}".format(dir=path))
            return True
        else:
            return False

    def check_log_store_dir(self):
        dirs = ['logs', 'store']
        for dir_name in dirs:
            print(dir_name)
            if self.check_dir_exists(dir_name) is False:
                print("created {dir_name}".format(dir_name=dir_name))
                return self.create_dir(dir_name)

    def create_dir(self, folder):
        """
        :return: creates directories
        """
        try:
            print("{path}/{folder}".format(path=self.file_path_levels(".."), folder=folder))
            return os.makedirs("{path}/{folder}".format(path=self.file_path_levels(".."), folder=folder))

        except FileExistsError:
            print("File Exists already")

    def create_image_store_dir(self, country, ad_uid):
        """
        :param country: code for the directory
        :param ad_uid: for the directory
        :return: complete directory path for image storage
        """

        try:
            year = datetime.now().strftime("%Y")
            month = datetime.now().strftime("%m")

            path = "{dir_loc}/store/{year}/{month}/{country}/" \
                   "{ad_uid}/".format(dir_loc=self.file_path_levels(".."),
                                      year=year,
                                      month=month,
                                      country=country,
                                      ad_uid=ad_uid)

            file_exist = self.check_dir_exists(path)
            print("----create_image_store_dir----")
            print(path)

            if not file_exist:
                os.makedirs(path)
                self.log.debug("Making directory: " + path)
                print("----file doesnot exists----")
                print(path)

                return path

        except (FileExistsError, TypeError):
            print("File already exists")
            pass

    def check_errors(self, file_name):
        """
        :param file_name: containing the image to scan for 404 page errors
        :return: True/False for page errors
        """

        validate_text = pytesseract.image_to_string(Image.open(file_name))
        search = re.search(r'404', validate_text)
        self.log.debug("Checking for errors, eg: 404...")
        if search:
            self.log.debug("Found 404 page error")
            return True

        elif validate_text is None:
            self.log.debug("Page empty, no adverts loaded")
            return True

        else:
            self.log.debug("No page errors found, all good")
            return False
