# -*- coding: utf-8 -*-
"""

connector.py

"""


from log import Log


class Connector (object):
    """
    The Connector class is used to access a data source.
    """

    def __init__(self, connect_params):
        """ Initialise instance. """

        # Access log.
        self.log = Log()
        self.log.set_context(self.__class__.__name__)

        # Connection params.
        self.connect_params = connect_params


    def connect(self):
        """
        Connects to the data source.

        :return: None
        """

        pass


    def close(self):
        """
        Closes the connection to the data source.

        :return: None
        """

        pass
