# -*- coding: utf-8 -*-
"""

mysql_connector.py

"""


import pymysql as mysql
from datetime import datetime
from src.utility.datetime import fmt_datetime, fmt_date
from funpy.funpy.connector.connector import Connector


def dbaction(func):
    """
        Executes the specified function with a connect and close.

        :param func: the function to execute
        :return: A wrapper function that calls the specified function.
    """

    def wrapper(self, *args, **kwargs):
        """
        The wrapper function that does the work.

        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: The result of calling a function or 'no_result' if the result is None, or False if
        any exceptions occurred.
        """

        try:

            return func(self, *args, **kwargs)

        except mysql.MySQLError as ex:

            if ex.args[0] == 1064:
                self.log.error('incorrect SQL [{sql}]'.format(sql=self.sql))
            else:
                self.log.error('SQL error [{error}]'.format(error=str(ex)))

    return wrapper


class MySQLConnector (Connector):
    """
    The MySQLConnector class is used to access a MySQL database.
    """

    def __init__(self, params):
        """ Initialise instance. """

        # Get the database connect params.
        connect_params = {key: value for key, value in params.items() if key != 'tables'}

        # Set char set if not set.
        if 'charset' not in connect_params:
            connect_params['charset'] = 'utf8mb4'

        # Set cursor class if not set.
        if 'cursorclass' not in connect_params:
            connect_params['cursorclass'] = mysql.cursors.DictCursor

        # Base class processing.
        Connector.__init__(self, connect_params)

        # Get the table map.
        self.tables = params['tables']

        # Connector.
        self.conn = None

        # Latest sql statement.
        self.sql = None


    def connect(self):
        """
        Connects to the specified database.

        :return: None
        """

        # Connect to the database.
        if self.conn is None:

            try:

                self.conn = mysql.connect(**self.connect_params)

            except Exception as ex:

                self.conn = None
                raise ex


    def close(self):
        """
        Closes the connection.

        :return: None
        """

        try:

            if self.conn:
                self.conn.close()

        except:

            pass

        finally:

            self.conn = None


    @dbaction
    def select(self, table, fields=None, locators=None, like=False, sorters=None, limit=None):
        """
        Selects fields from the specified table where the locator fields are equal to the locator values, limiting the
        number of rows selected.

        :param table: the table
        :param fields: the fields to select
        :param locators: a list of field/value pairs as a dictionary
        :param limit: the maximum number of rows to update
        :return: a dictionary
        """

        # Get table.
        table = self.format_table(table)

        # Build SQL command.
        self.sql = 'select {fields} from {table}'.format(fields=self.format_fields(fields), table=table)

        # Execute command.
        return self.execute_read(self.sql, locators, like, sorters, limit)


    @dbaction
    def insert(self, table, inserts, inc_time=False):
        """
        Inserts a new record into the specified table with fields and values as specified in the inserts.

        :param table: the table
        :param updates: a list of field/value pairs as a dictionary
        :param locators: a list of field/value pairs as a dictionary
        :return: The last row id if successful, or False otherwise
        """

        # Get table.
        table = self.format_table(table)

        # Build SQL command.
        self.sql = 'insert into {table} ({keys}) values ({values})'.format(table=table,
            keys=','.join(inserts.keys()), values=self.format_value_list(inserts.values(), inc_time=inc_time))

        # Execute command.
        return self.execute_write(self.sql, None)


    @dbaction
    def update(self, table, updates, locators=None):
        """
        Updates the specified table by setting the update fields to the update values where the locator fields are
        equal to the locator values.

        :param table: the table
        :param updates: a list of field/value pairs as a dictionary
        :param locators: a list of field/value pairs as a dictionary
        :return: The last row id if successful, or False otherwise
        """

        # Get table.
        table = self.format_table(table)

        # Build SQL command.
        self.sql = "update {table} set {updates}".format(table=table, updates=self.format_field_value_list(updates))

        # Execute command.
        return self.execute_write(self.sql, locators)


    @dbaction
    def increment(self, table, field, locators=None, increment=1):
        """
        Increment the field in the specified table by a specified amount.

        :param table: the table
        :param field: the field
        :param locators: a list of field/value pairs as a dictionary
        :param increment: the amount to add
        :return: The last row id if successful, or False otherwise
        """

        # Get table.
        table = self.format_table(table)

        # Build SQL command.
        self.sql = "update {table} set {field}={field}+{increment}".format(table=table, field=field, increment=increment)

        # Execute command.
        return self.execute_write(self.sql, locators)


    @dbaction
    def column_exists(self, table, name):
        """
        Determines if a column with the specified name is in the specified table.

        :param table: the table name
        :param name: the column name
        :return: bool
        """

        # Get table.
        table = self.format_table(table)

        # Build SQL command.
        self.sql = "show columns from {table} like {name}".format(table=table, name=self.format_mysql(name))

        # Create cursor and execute command.
        with self.conn.cursor() as cursor:
            res = cursor.execute(self.sql)
            return bool(res)


    @dbaction
    def table_exists(self, table):
        """
        Determines if a table is in the database.

        :param table: the table name
        :return: bool
        """

        # Get table.
        table = self.format_table(table)

        # Build SQL command.
        self.sql = "show tables like {table}".format(table=self.format_mysql(table))

        # Create cursor and execute command.
        with self.conn.cursor() as cursor:
            res = cursor.execute(self.sql)
            return bool(res)


    def format_table(self, table):
        """
        Formats the table if a dictionary of table names is present.

        :param table: a table key
        :return: a table name
        """

        return self.tables.get(table, table)


    def format_fields(self, fields):
        """
        Returns a string as a formatted list of fields.

        :param fields: a list of fields
        :return: a string
        """

        if fields:
            return ','.join([str(field) for field in fields])

        return '*'


    def format_mysql(self, value, quote=True, inc_time=False):
        """
        Returns the value formatted to insert into a SQL command.

        :param value: a value
        :return: the value quoted or not, as necessary
        """

        if value is None:
            return 'NULL'
        elif isinstance(value, str) and quote:
            return "'{value}'".format(value=value)
        elif isinstance(value, datetime) and quote:
            if inc_time:
                return "'{value}'".format(value=value.strftime(fmt_datetime))
            else:
                return "'{value}'".format(value=value.strftime(fmt_date))
        else:
            return str(value)


    def format_field_value_list(self, field_values, join_str=',', relation='=', quote=True, inc_time=False):
        """
        Returns a string as a formatted list of field=value pairs, separated by the specified join string.

        :param field_values: a dictionary of field/value pairs.
        :param join_str: a string to connect all the field=value pairs
        :return: a string
        """

        pairs = []
        for key, value in field_values.items():
            pair_value = self.format_mysql(value, quote, inc_time)
            if pair_value == 'NULL':
                pairs.append('{key}{relation}{value}'.format(key=key, relation=' is ', value=pair_value))
            else:
                pairs.append('{key}{relation}{value}'.format(key=key, relation=relation, value=pair_value))

        return join_str.join(pairs)


    def format_value_list(self, values, join_str=',', quote=True, inc_time=False):
        """
        Returns a string as a formatted list of values, separated by the specified join string.

        :param items: a list of values
        :param join_str:
        :return: a string
        """

        return join_str.join([ self.format_mysql(value, quote, inc_time) for value in values])


    def append_where(self, sql, locators, like=False):
        """
        Appends a where clause to the sql.

        :param sql: the original sql
        :param locators: a list of locators
        :return: a string
        """

        if locators:
            if like:
                relation = ' like '
            else:
                relation = '='

            sql = '{sql} where {locators}'.format(sql=sql,
                locators=self.format_field_value_list(locators, ' and ', relation))

        return sql


    def append_order(self, sql, sorters):
        """
        Appends an order by clause to the sql.

        :param sql: the original sql
        :param locators: a list of sorters
        :return: a string
        """

        if sorters:
            sorters = {key : 'asc' if value else 'desc' for key, value in sorters.items()}
            sql = '{sql} order by {sorters}'.format(sql=sql,
                sorters=self.format_field_value_list(sorters, relation=' ', quote=False))

        return sql


    def append_limit(self, sql, limit):
        """
        Appends a limit to the sql if it is sensible.

        :param sql: the original sql
        :param limit: the maximum number of rows to return
        :return: a string
        """

        if limit and (limit > 0):
            return '{sql} limit {limit}'.format(sql=sql, limit=limit)

        return sql


    def execute_write(self, sql, locators):
        """
        Executes an SQL insert or update command.

        :param sql: the sql to execute
        :return: The last row id of the command if something was changed, or False.
        """

        # Add where to query if necessary.
        sql = self.append_where(sql, locators)

        with self.conn.cursor() as cursor:
            if cursor.execute(sql) > 0:
                self.conn.commit()
                return cursor.lastrowid if cursor.lastrowid else True

            return False


    def execute_read(self, sql, locators, like, sorters, limit):
        """
        Executes an SQL select command.

        :param sql: the sql to execute
        :param limit: the maximum number of rows to return
        :return: list of rows as a dict
        """

        # Add where to query if necessary.
        sql = self.append_where(sql, locators, like)

        # Add order by to query if necessary.
        sql = self.append_order(sql, sorters)

        # Add limit to query if necessary.
        sql = self.append_limit(sql, limit)
        if not sql:
            return []

        # Create cursor and execute command.
        with self.conn.cursor() as cursor:
            res = cursor.execute(sql)
            if not res or (limit and (limit < res)):
                return []

            return cursor.fetchall()
