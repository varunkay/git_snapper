from selenium import webdriver
from pathlib import Path


def browser(file_path, web_driver):
    """
    :param file_path:
    :param web_driver: location of chrome driver binary
    :return: driver settings to run the snapper
    """
    # Set up chrome driver configuration
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--headless")

    # Set preferences for chrome for urls with direct link to downloads
    prefs = {"download.default_directory": file_path}
    chrome_options.add_experimental_option("prefs", prefs)

    # Initialize chrome driver
    driver = webdriver.Chrome(executable_path=web_driver,
                              chrome_options=chrome_options)

    return driver
