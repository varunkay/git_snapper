import time
import ssl
from urllib import request


def image_iterator():
    for i in range(1, 4):
        return i


def advert_without_image_tag(driver, sleep_time, image_processor, ad_src, image_type, file_path, file):
    """
    :param driver: Initiate chrome driver
    :param sleep_time: waiting time for snapper
    :param image_processor: calls this method to process the image
    :param ad_src: url containing advert
    :param image_type: type of image
    :param file_path: location to store the image file
    :param file: Method responsible for creating/ maintaining directories
    :return: size_params for image size
    """
    try:
        driver.get(ad_src)
        time.sleep(sleep_time)

        # xpath to locate image using img tag
        element = driver.find_element_by_xpath("//img")

        size_params = image_processor.size_params(element)

        # used to fix ssl error while scrapping
        ssl._create_default_https_context = ssl._create_unverified_context

        # Be careful, reason why its written again!! because of file type
        if ad_src.split(".")[-1] in image_type:
            for i in range(1, 4):
                # file_name = ad_src.split("/")[-1]
                file_name = "index_{}".format(i)
                full_path = "{file_path}/{file_name}".format(file_path=file_path,
                                                             file_name=file_name)
                request.urlretrieve(ad_src, full_path)

                check_errors = file.check_errors(file_name)
                if check_errors:
                    print("rejected/ page not found")
                    # log.debug("404 Page error found")
                    pass

        else:
            for i in range(1, 4):
                # file_name = ad_src.split("/")[-1]
                file_name = "index_{}".format(i)
                full_path = "{file_path}/{file_name}.jpeg".format(file_path=file_path,
                                                                  file_name=file_name)

                request.urlretrieve(ad_src, full_path)
            return size_params

    except Exception:
        # log.error("Image directly downloaded. {dir} directory".format(dir=ad_src))
        pass
