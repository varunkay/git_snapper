import time
from file import File
from image_processor.image_processor import ImageProcessor
from logging_settings import logging_settings


def advert_with_image_tag(driver, file_path, sleep_time, ad_src):
    """
    :param driver: Initiate chrome driver
    :param file_path: to store the image
    :param sleep_time: wait period for the snap to finish
    :param ad_src: url for the advert
    :return: size_params for size of the image taken
    """
    # Driver gets advert url
    driver.get(ad_src)

    # waiting time for image to load/process more is good
    time.sleep(sleep_time)

    # Using .png instead of .jpg due to compatibility issues with Pillow for processing
    file_name = "index_1.png"

    full_path = "{file_path}{file_name}".format(file_path=file_path, file_name=file_name)

    print("..........debug............")
    print(file_path)
    print(file_name)
    print("...........................")

    # TODO put in advert_snapper.py
    log = logging_settings()
    image_processor = ImageProcessor()
    file = File()

    # fetch elements by div tag if exists
    try:
        if driver.find_element_by_xpath("//div"):
            element = driver.find_element_by_xpath("//div")
            size_params = image_processor.size_params(element)

            # take screen shot
            driver.save_screenshot(full_path)
            image_processor.crop_background(full_path, sleep_time)

            return size_params

        else:
            check_errors = file.check_errors(full_path)
            driver.save_screenshot(full_path)
            if check_errors:
                log.debug("404 Page error found")
                pass

    except:
        print("File already exists...")
        pass
