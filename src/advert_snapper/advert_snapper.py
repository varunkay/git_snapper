from datetime import datetime
from logging_settings import logging_settings
from file import File
from image_processor.image_processor import ImageProcessor
from model import Model
from .advert_with_image_tag import advert_with_image_tag
from .advert_without_image_tag import advert_without_image_tag


# TODO NB: sql queries values are processed in list and then opened


class AdvertSnapper(object):
    """
    AdvertSnapper class gets the advert from the url and runs appropriate webdriver
    at set intervals
    """

    def __init__(self):

        # Create model instance/ change to connector
        self.QUERY = Model()

        # create new file instance
        self.file = File()

        # Creates new logging instance
        self.log = logging_settings()

        # self.connector = Connector(CONFIG_PATH)
        # self.mysql = MySQLConnector(self.connector)

        # Creates image_processor instance
        self.image_processor = ImageProcessor()

    # TODO add url parameter to get_image
    def fetch_image(self, adverts, interval, count, index, file_path, driver):
        """
        :return: advert_dict containing image info from processed url and handled accordingly
        """

        check_errors = False
        full_path = ""
        file_name = ""

        # Retrieves Snapper settings from snapper_settings table
        # TODO Interval comes from snapper.py, but needs to come from snapper_settings table db

        # select sleep_time
        sleep_time = interval

        site_type = ['html', 'htm', 'php', 'aspx']
        image_type = ['jpg', 'jpeg', 'png', 'gif', 'tif']

        if count == 0:
            print("** All adverts have been processed **")

        count_remaining = len(self.QUERY.select_unprocessed_ads())
        print("///////////////////////////////////////////////////////////////")
        print("Unprocessed adverts remaining: {count}".format(count=count_remaining))
        print("///////////////////////////////////////////////////////////////")

        self.log.debug("index: {}".format(index))

        # eg, ad_src: "https://cdn.flashtalking.com/91121/2247613/index.html"
        ad_src = adverts['ad_src']

        print("-------------")
        print(file_path)
        print("-------------")

        self.log.debug(file_path)
        self.log.debug("url: {}".format(ad_src))

        # check file type from url and match it to known types
        if ad_src.split(".")[-1] in site_type:

            # Navigates to advert_with_image_tag method, downloads image directly to directory
            size_params = advert_with_image_tag(driver=driver, file_path=file_path, sleep_time=sleep_time, ad_src=ad_src)

        else:
            print("## For images without specific details on image type ##")

            # Runs method for adverts which do not have any image tags so takes screen shot returns size params
            size_params = advert_without_image_tag(driver=driver, sleep_time=sleep_time,
                                                   image_processor=self.image_processor, ad_src=ad_src,
                                                   image_type=image_type, file_path=file_path, file=self.file)

        if size_params is None:
            size_params = [adverts['ad_width'], adverts['ad_height']]

        # slicing path: '2018/10/UK/87d07edf-f447-4732-aabe-47aa7/88885.gif'
        full_path = "/".join(full_path.split("/")[-5:])

        # Can get from table
        ext = adverts['ad_finfo'].split("/")[1]

        advert_dict = {
            "ad_id": adverts['ad_id'],
            "ad_src": ad_src,
            "ad_uid": adverts['ad_uid'],
            "timestamp": datetime.now(),
            "ext": ext,
            "width": size_params[0],
            "height": size_params[1],
            "debug_data": adverts,
            "file_name": file_name,
            "file_path": file_path,
            "full_path": full_path,
            "domain": adverts['domain'],
            "ad_finfo": adverts['ad_finfo'],
            "ad_rejected": check_errors,
        }

        print(advert_dict)

        # TODO REMOVE BEFORE PRODUCTION - DEBUG ONLY
        print("Processing advert...")
        print("ad_id: {}".format(adverts['ad_id']))
        print("ad_src: {}".format(ad_src))
        print("width: {}px".format(size_params[0]))
        print("height: {}px".format(size_params[1]))
        print("debug_data: {}".format(adverts))
        print("file directory: {}".format(full_path))
        print("domain: {}".format(adverts['domain']))
        print("ad_info: {}".format(adverts['ad_finfo']))
        print("Advert Rejected: {rejection}".format(rejection=check_errors))
        print("ad_id set to processed")

        print(" ")

        # pipeline.append(advert_dict)

        driver.close()
        driver.quit()

        return advert_dict
    # For debugging purposes
    # # print(pipeline)
    # print("///////////////////////////////////////////////////////////////")
    # print("Waiting...")
    # print("///////////////////////////////////////////////////////////////")
