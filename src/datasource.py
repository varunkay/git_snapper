# -*- coding: utf-8 -*-
"""

datasource.py

# This is essentially a database data source and there will be one which makes API calls, at which point
# this class will be renamed.

"""


import json
from src.connector.mysql_connector import MySQLConnector
from login import Login
from src.utility.general import get_value_as_type, check_value
from src.utility.dictionary import set_object_from_dict, get_sub_dict
from src.utility.tree import ListTree


class DatasourceException (Exception):
    pass


class Datasource (object):
    """
    The Datasource class has methods for accessing data needed for the hunter.
    """

    def __init__(self, params):
        """ Initialise instance. """

        # Create a connector.
        self.connector = MySQLConnector(params['database'])

        # Test the connector.
        self.connector.connect()
        self.connector.close()

        # Internal data cache.
        # Todo: the first two should be some sort of empty structure.
        self.placements = None
        self.adserver_domains = None
        self.custom_user_agent_url_ids = []


    def open(self):
        """
        Opens the datasource.

        :return:
        """

        self.connector.connect()


    def close(self):
        """
        Closes the datasource.

        :return:
        """

        self.connector.close()


    def update(self):
        """
        Updates the internal data.

        :return:
        """

        # Placements.
        self.update_placements()

        # Adserver domains.
        self.update_adserver_domains()

        # Urls ids that need a custom user agent.
        self.update_custom_user_agent_url_ids()


#
# Config.
#

    def get_sat_config(self):
        """
        Returns the sat congif info.

        :return: dict
        """


        items = self.connector.select('sat_config', ['dict_key', 'dict_value', 'value_type'], locators={'enabled': 1})
        return {item['dict_key']: get_value_as_type(item['dict_value'], item['value_type']) for item in items} if items else {}


    def get_hunter_settings(self):
        """
        Returns the hunter app settings.

        :return:
        """

        items = self.connector.select('hunter_settings', ['dict_key', 'dict_value', 'value_type'], locators={'enabled': 1})
        return {item['dict_key']: get_value_as_type(item['dict_value'], item['value_type']) for item in items} if items else {}


#
# Placements.
#

    def update_placements(self):
        """
        Creates placement search trees.

        :return: None
        """

        placements = self.connector.select('placements', ['width', 'height'])

        self.placements = {}
        for placement in placements:

            for w in [-1, 0, 1]:
                if not (placement['width'] + w) in self.placements:
                    self.placements[placement['width'] + w] = {}

                for h in [-1, 0, 1]:
                    self.placements[placement['width'] + w][placement['height'] + h] = ((w == 0) and (h == 0))


    def is_known_placement(self, width, height):
        """
        Determines if images dimensions match a known placement.

        NB. To determine a single boolean of an exact placement match, call the method with all()

        :param width: the width
        :param height: the height
        :return: boolean, boolean (True if matches exactly or False if within tolerance)
        """

        try:
            return True, self.placements[width][height]

        except:
            return False, None


#
# Adservers.
#

    def update_adserver_domains(self):
        """
        Creates adserver domain search trees.

        :return: None
        """

        # Create adserver domain comparison trees.
        self.adserver_domains = [[ListTree(), ListTree()],
                                 [ListTree(), ListTree()]]

        base_domains_by_id = {domain['id']: domain['domain'].split('.')
            for domain in self.connector.select('adserver_domains', ['id', 'domain'])}
        base_domains_by_id_ci = {domain['id']: domain['domain'].casefold().split('.')
            for domain in self.connector.select('adserver_domains', ['id', 'domain'])}

        self.adserver_domains[0][0].add_lists(base_domains_by_id_ci.values())
        self.adserver_domains[0][1].add_lists(base_domains_by_id.values())

        domains = self.connector.select('adservers', ['subdomain', 'domain_id'])
        for domain in domains:

            full_domain = []
            full_domain_ci = []

            if domain['subdomain']:
                full_domain.extend(domain['subdomain'].split('.'))
                full_domain_ci.extend(domain['subdomain'].casefold().split('.'))
            full_domain.extend(base_domains_by_id[domain['domain_id']])
            full_domain_ci.extend(base_domains_by_id_ci[domain['domain_id']])

            self.adserver_domains[1][0].add_list(full_domain_ci)
            self.adserver_domains[1][1].add_list(full_domain)


    def domain_contains_adserver_domain(self, domain, full_adserver_domain=True, case_sensitive=False):
        """
        Determines if the specified domain contains an adserver domain.

        :param domain: a domain
        :param full_adserver_domain: True to use the tree of full adserver domains or False to use the base domains
        :param case_sensitive: True to use case insensitive comparison or False otherwise
        :return: boolean
        """

        if not case_sensitive:
            domain = domain.casefold()

        return self.adserver_domains[int(full_adserver_domain)][int(case_sensitive)].is_contained_in(domain.split('.'))


#
# Custom user agent urls.
#

    def update_custom_user_agent_url_ids(self):
        """
        Creates list of url ids that require a custom user agent.

        :return: None
        """

        url_ids = self.connector.select('urls', ['u_id'], {'u_url' : '%citywire%'}, True, )
        if url_ids:
            self.custom_user_agent_url_ids = [url_id['u_id'] for url_id in url_ids]


#
# Login.
#

    def get_login(self, url_id):
        """
        Returns the login object for the specified url id.

        :param url_id: the url id
        :return: a login object
        """

        login_id = self.connector.select('url_login',
            fields=['login_id'], locators={'url_id': url_id, 'active': 1}, limit=1)

        try:
            login_id = login_id[0]['login_id']
        except:
            return

        login_info = self.connector.select('logins',
            fields=[
                'login_button_xpath',
                'user_field_xpath',
                'user_next_button_xpath',
                'password_field_xpath',
                'submit_button_xpath',
                'logged_in_element_close_xpath',
                'success_element_xpath',
                'user',
                'password'
            ],
            locators={'id': login_id},
            limit=1)

        if login_info:

            try:
                login = Login()
                set_object_from_dict(login, login_info[0])
                return login
            except:
                pass


#
# Lightbox.
#

    def get_lightbox_info(self, url_id):
        """
        Returns the lightbox info for the specified url.

        :param url_id: the url id
        :return: dict
        """

        lightbox_id = self.connector.select('url_lightbox',
            fields=['lightbox_id'], locators={'url_id': url_id, 'active': 1}, limit=1)

        try:
            lightbox_id = lightbox_id[0]['lightbox_id']
        except:
            return ''

        close_xpath = self.connector.select('lightboxes',
            fields=['close_xpath', 'frame_xpath'], locators={'id': lightbox_id}, limit=1)

        try:
            return close_xpath[0]
        except:
            return {}


#
# Page info.
#

    def get_page_info(self):
        """
        Returns info for all pages.

        :return: list of dictionaries (page infos)
        """

        try:

            return self.connector.select('urls',
                ['u_id', 'target', 'qualifier', 'audience', 'u_url', 'cycles_limit'], locators={'status': 0})

        except Exception as ex:
            raise DatasourceException('get_page_info ({exception})'.format(exception=str(ex)))


#
# Cycles.
#

    def get_cycle_count(self, url_id, date):
        """
        Returns the daily cycles for the specified url and date.

        :param url_id: a url id
        :param date: a date
        :return: the daily cycles
        """

        row = self.connector.select('cycles', ['cycles'], {'u_id': url_id, 'date': date}, 1)
        if row and ('cycles' in row[0]) and row[0]['cycles']:
            return row[0]['cycles']
        else:
            return 0


    def set_cycle_count(self, url_id, date, cycles):
        """
        Sets the daily cycles for the specified url and date.

        :param url_id: the url id
        :param date: the date
        :param cycles: the cycles
        :return: True if successful, of False otherwise
        """

        # Check if there is an entry for the specified url.
        id = self.connector.select('cycles', ['id'], {'u_id': url_id, 'date': date})

        if not id:
            return self.connector.insert('cycles', {'u_id': url_id, 'cycles': cycles, 'date': date})
        else:
            return self.connector.update('cycles', {'cycles': cycles}, {'u_id': url_id, 'date': date}, 1)


    def update_cycle_count(self, url_id, date):
        """
        Increments the daily cycles for the specified url and date by 1.

        :param url_id: the url id
        :param date: the date
        :return: True if successful, of False otherwise
        """

        # Check if there is an entry for the specified url.
        id = self.connector.select('cycles', ['id'], {'u_id': url_id, 'date': date})

        if not id:
            return self.connector.insert('cycles', {'u_id': url_id, 'cycles': 1, 'date': date})
        else:
            return self.connector.increment('cycles', 'cycles', {'u_id': url_id, 'date': date}, 1)


#
# Adverts.
#

    def find_existing_advert(self, source_search_string):
        """
        Returns the earliest advert where the source matches the source search string.

        :param source_search_string: a source search string
        :return: a dictionary
        """

        return self.connector.select(
            'adverts',
            ['ad_id', 'ad_uid', 'domain'],
            { 'ad_src': source_search_string }, True,
            {'datetime': True}, limit=2)


    def add_advert(self, advert, country, hunter_version=None):
        """
        Inserts an advert into the database.

        :param advert: an advert
        :return: The id of the newly inserted advert, or None
        """

        inserts = {
            'ad_uid': advert.unique_id,
            'ad_src': advert.source,
            'ad_width': advert.size[0],
            'ad_height': advert.size[1],
            'ad_finfo': advert.content_type,
            'ad_server': advert.adserver,
            'domain': advert.advertiser_domain if advert.advertiser_domain else None,
            'ad_landing': advert.advertiser_landing if advert.advertiser_landing else None,
            'datetime': advert.datetime,
            'x': advert.location[0],
            'y': advert.location[1],
            'net_id': advert.net_id,
            'cmp_id': advert.campaign_id,
            'pla_id': advert.placement_id,
            'adv_id': advert.advertiser_id,
            'svr_src': None,
            'ad_type': advert.classification.value,
            'iframe': 1 if advert.is_frame else 0,
            'country': country,
            'landing_method': advert.landing_method.value,
            'hunter_version': hunter_version,
        }

        id = self.connector.insert('adverts', inserts, inc_time=True)
        if id:
            return id


    def add_advert_full_source(self, advert):
        """
        Inserts an advert full source into the database.

        :param advert: an advert
        :return: True and the id of the newly inserted source, or False and an error message.
        """

        inserts = {
            'advert_uid': advert.unique_id,
            'source': advert.full_source,
            'type': advert.full_source_type.value,
            }

        id = self.connector.insert('advert_sources', inserts)
        if id:
            return id


    def add_advert_words(self, word_info):
        """
        Adds adverts words.

        :param word_info: word information
        :return:
        """

        id = self.connector.insert('advert_words', get_sub_dict(word_info, ['advert_uid', 'words', 'source_type']))
        if id:
            return id


    def update_advert_landing_info(self, advert):
        """
        Updates the landing values on the specified advert.

        :param advert: an advert
        :return: boolean
        """

        # Update domain.
        updates = {
            'domain': advert.advertiser_domain,
        }

        # Update landing.
        if advert.advertiser_landing:
            updates['ad_landing'] = advert.advertiser_landing

        # Update landing method.
        if self.connector.column_exists('adverts', 'landing_method'):
            updates['landing_method'] = advert.landing_method.value

        locators = {
            'ad_id': advert.id,
            'ad_uid': advert.unique_id,
        }

        return self.connector.update('adverts', updates, locators)


#
# Advert instances.
#

    def get_instance(self, advert):
        """
        Determines if there is an existing instance for the ad, ad unique id, url and date.

        :param advert:
        :return:
        """

        locators = {
            'ad_id': advert.id,
            'ad_uid': advert.unique_id,
            'u_id': advert.url_id,
            'date': advert.datetime
        }

        row = self.connector.select('instances', ['id'], locators)
        if row and ('id' in row[0]) and row[0]['id']:
            return row[0]['id']

        return False


    def add_instance(self, advert):
        """
        Adds a new instance.

        :param advert: an advert
        :return: boolean
        """

        inserts = {
            'ad_id': advert.id,
            'ad_uid': advert.unique_id,
            'u_id': advert.url_id,
            'counter': advert.instances,
            'date': advert.datetime
        }

        return self.connector.insert('instances', inserts)


    def increment_instance(self, advert):
        """
        Increments the instance count for the specified advert.

        :param advert: an advert
        :return: boolean
        """

        locators = {
            'ad_id': advert.id,
            'ad_uid': advert.unique_id,
            'u_id': advert.url_id,
            'date': advert.datetime,
        }

        return self.connector.increment('instances', 'counter', locators, advert.instances)


    def add_rejected_container(self, container, url_id, datetime):
        """

        :param container:
        :return:
        """

        # details = container.get_details(['element', 'container_type', 'xpath', 'frame'])
        #
        # if self.connector.table_exists('rejected_containers'):
        #
        #     inserts = {
        #         'url_id': url_id,
        #         'details': json.dumps(details),
        #         'container_type': container.container_type.value,
        #         'datetime': datetime
        #     }
        #
        #     id = self.connector.insert('rejected_containers', inserts)
        #     if id:
        #         return id

        pass
