# -*- coding: utf-8 -*-
"""

log.py

"""


import logging
from os import path
# from config import Config
from configparser import ConfigParser

from src.utility.log import (get_log_file_path, add_creating_file_handler, add_stream_handler,
                         add_sentry_io_handler, close_logger)
from src.utility.datetime import fmt_datetime, fmt_date_compact


class Log (object):
    """
    The Log class enables logging functionality.
    """

    # Class params.
    params = {}

    def __init__(self, class_params=None):
        """ Initialise instance. """

        # Access config data.
        self.cfg = ConfigParser()
        self.cfg.read('/Users/varun/git_workspace_FM/snapper2/config/snapper_settings.ini')

        # self.cfg = Config('hunter')

        # Create logger.
        self.logger = logging.getLogger(self.cfg.get('settings.log.name', 'snapper'))

        # Set up class data.
        if not class_params is None and not Log.params:
            Log.params = {key: value for key, value in class_params.items()}

        # Modifiable local copy of class data.
        self.extra = {
            'version': self.params.get('version', '0.0.0.0000'),
            'context': self.params.get('context', 'None'),
            'sat_id': self.params.get('sat_id', '0'),
            'country': self.params.get('country', '??')}


    def open(self, date):
        """
        Initialises the log from the config settings.

        :return: None
        """

        # Set logging level.
        level = logging.DEBUG if self.cfg.get('settings.log.verbose') else logging.INFO
        self.logger.setLevel(level)

        # Set formatter.
        formatter = logging.Formatter('[{asctime}][{version}][{country}][{sat_id:>3}][{context:>20}][{levelname:.1}] {message}',
            datefmt=fmt_datetime, style='{')

        # Create the file handler if specified.
        if self.cfg.get('settings.log.file_enabled', False):

            # Get log file path.
            log_file_path = get_log_file_path(
                path.join(self.params['instance_path'], self.cfg.get('settings.log.path', 'logs')),
                name=self.cfg.get('settings.log.name', 'snapper'), date=date, date_format=fmt_date_compact,
                ext=self.cfg.get('settings.log.file_ext', 'log'))

            # Create and add file handler.
            if log_file_path:
                add_creating_file_handler(self.logger, log_file_path, formatter, level)

        # Create the stream handler if specified.
        if self.cfg.get('settings.log.stream_enabled', False):
            add_stream_handler(self.logger, formatter, level)

        # Create the sentry.io handler if specified.
        if self.cfg.get('settings.log.sentry_enabed', False):
            add_sentry_io_handler(self.logger,
                'https://d84069d81c48478da088802c7c1f7c0a:f623325b38a44bb4ae86bd23c872efc8@sentry.io/1196176',
                formatter, logging.ERROR)

        self.info('log opened')


    def set_context(self, context):
        """
        Sets the log context.

        :param context:
        :return:
        """

        self.extra['context'] = context


    def info(self, *args, **kwargs):
        """
        Passes info to the logger.

        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: None
        """

        logging.LoggerAdapter(self.logger, self.extra).info(*args, **kwargs)


    def error(self, *args, **kwargs):
        """
        Passes error to the logger.

        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: None
        """

        logging.LoggerAdapter(self.logger, self.extra).error(*args, **kwargs)


    def warning(self, *args, **kwargs):
        """
        Passes warning to the logger.

        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: None
        """

        logging.LoggerAdapter(self.logger, self.extra).warning(*args, **kwargs)


    def debug(self, *args, **kwargs):
        """
        Passes debug to the logger.

        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: None
        """

        logging.LoggerAdapter(self.logger, self.extra).debug(*args, **kwargs)


    def critical(self, *args, **kwargs):
        """
        Passes critical to the logger.

        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: None
        """

        logging.LoggerAdapter(self.logger, self.extra).critical(*args, **kwargs)


    def exception(self, *args, **kwargs):
        """
        Passes critical to the logger.

        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: None
        """

        logging.LoggerAdapter(self.logger, self.extra).exception(*args, **kwargs)


    def close(self):
        """
        Close down the log (remove file handles, etc).

        :return: None
        """

        self.info('log closed')
        close_logger(self.logger)
