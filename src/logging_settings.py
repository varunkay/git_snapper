import logging
from datetime import datetime
from pathlib import Path


filename = datetime.now().strftime("{dir_loc}/logs/%Y-%m-%d_snapper.log".format(
                                    dir_loc=Path(__file__).parents[1]))


FORMATTER = logging.Formatter('%(asctime)s - %(filename)s:%(lineno)s - %(funcName)20s() - %(message)s')
LEVEL = logging.DEBUG


def logging_settings():
    """
    :return: logger currently used, to be replaced by funpy log
    """
    logger = logging.getLogger("SnapperLogger")
    handler = logging.FileHandler(filename)
    handler.setFormatter(FORMATTER)
    logger.addHandler(handler)
    logger.setLevel(LEVEL)
    return logger


