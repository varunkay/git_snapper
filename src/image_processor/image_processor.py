import time
from PIL import Image, ImageChops
from logging_settings import logging_settings


class ImageProcessor(object):

    def __init__(self):

        self.log = logging_settings()

    def trim(self, im, border):
        """
        :param im: image insert
        :param border: background colour of DOM, set to White
        :return: crop image without the background
        """

        background = Image.new(im.mode, im.size, border)
        diff = ImageChops.difference(im, background)
        background_box = diff.getbbox()
        if background_box:
            return im.crop(background_box)

    def crop_background(self, dir_path, sleep_time):
        """
        :param dir_path: for the image
        :param sleep_time: selection from [3, 5, 15] sec
        :return: crop image and save
        """

        time.sleep(sleep_time)
        im = Image.open(dir_path)
        im = self.trim(im, border="white")
        rgb_im = im.convert('RGB')
        rgb_im.save(dir_path.replace("png", "jpeg"))
        self.log.debug("dir_path" + dir_path)

    def size_params(self, element):
        """
        :param element: from the chrome driver
        :return: list/array [width, height]
        """
        location = element.location
        size = element.size
        width = location['x'] + size['width']
        height = location['y'] + size['height']

        return [width, height]
