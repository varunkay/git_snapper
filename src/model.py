import pymysql as MySQLdb
from configparser import ConfigParser
from pathlib import Path

DIR_PATH = Path(__file__).parents[1]


class Model(object):
    """
    Database related queries
    """
    def _connect(self, query):
        """
        :param query: To execute specified query
        :return: data from database
        """
        config = ConfigParser()

        config.read('{dir_loc}/config/snapper_settings.ini'.format(dir_loc=DIR_PATH))

        db = MySQLdb.connect(host=config.get("DEFAULT", "DB_HOST"),
                             user=config.get("DEFAULT", "DB_USER"),
                             passwd=config.get("DEFAULT", "DB_PASS"),
                             db=config.get("DEFAULT", "DB_NAME"),
                             cursorclass=MySQLdb.cursors.DictCursor,
                             autocommit=True)
        try:
            cursor = db.cursor()
            cursor.execute(query)
            db.commit()
            db.close()
            return cursor.fetchall()

        except KeyboardInterrupt:
            db.commit()
            db.close()

    def select_unprocessed_ads(self):
        """
        :return: select query to find advert entries with processed = 0
        """
        return self._connect("SELECT * FROM Adverts where processed=0 and ad_id IS NOT NULL;")

    def select_advert_storage(self, uid):
        return self._connect("SELECT * FROM AdvertsStorage where uid={uid};".format(uid=uid))

    def update_unprocessed_ads(self, ad_id):
        """
        :param ad_id: for advert entry ID.
        :return: Update query setting processed = 1 after being captured
        """
        return self._connect('UPDATE Adverts SET processed=1 WHERE ad_id={};'.format(ad_id))

    def check_ad_exists_advert_storage(self, uid_dir):
        return self._connect("SELECT count(uid_dir) AS counter FROM AdvertsStorage WHERE uid_dir='%s';" % uid_dir)

    def select_from_snapper_settings(self):
        """
        :return: retrieves required settings for snapper
        """
        return self._connect("SELECT * FROM snapper_settings;")

    def insert_adverts_advertstorage(self, pipeline):
        """
        :param pipeline: List of dictionary containing key items.
        :return: Inserts the data from the dictionary to AdvertStorage table
        """
        return self._connect("INSERT INTO AdvertsStorage (uid_dir, timestamp, filename, ext,"
                             " fullpath, last_modified, created_at)"
                             " VALUES ('{uid_dir}', '{timestamp}', '{filename}', '{ext}',"
                             " '{fullpath}', '{last_modified}', '{created_at}');"
                             .format(uid_dir=pipeline['debug_data']['ad_uid'],
                                     timestamp=pipeline['timestamp'],
                                     filename=pipeline['file_name'],
                                     ext=pipeline['ext'],
                                     fullpath=pipeline['full_path'],
                                     last_modified=pipeline['timestamp'],
                                     created_at=pipeline['debug_data']['created']))

    def update_adverts_AdvertStorage(self, pipeline):
        """
        :param pipeline: dictionary containing the data to be inserted
        :return: Updates the data entries by uid_dir that needs to be updated.
        """
        return self._connect("UPDATE AdvertsStorage SET uid_dir='{uid_dir}', timestamp='{timestamp}',"
                             " filename='{file_name}', ext='{ext}', fullpath='{full_path}',"
                             " last_modified='{last_modified}', created_at='{created_at}'"
                             " WHERE uid_dir='{uid_dir}';".format(uid_dir=pipeline['debug_data']['ad_uid'],
                                                                  timestamp=pipeline['timestamp'],
                                                                  file_name=pipeline['file_name'],
                                                                  ext=pipeline['ext'],
                                                                  full_path=pipeline['full_path'],
                                                                  last_modified=pipeline['timestamp'],
                                                                  created_at=pipeline['debug_data']['created']))



