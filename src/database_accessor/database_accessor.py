
from model import Model
from logging_settings import logging_settings


class DBAccessor(object):
    """
    Object executes queries
    """
    def __init__(self):

        self.QUERY = Model()
        self.log = logging_settings()

    def execute_query(self, adverts, advert_dict):
        # TODO sort queries out using funpy or create generic queries
        try:
            # Updating table Adverts setting processed=1
            self.QUERY.update_unprocessed_ads(adverts['ad_id'])
            self.log.debug("fetch_unprocessed_url ")

        except:
            self.log.error("Update processed=1 failed, adverts table")
            pass

        try:
            if self.QUERY.check_ad_exists_advert_storage(adverts["ad_uid"])[0]['counter']:
                self.QUERY.update_adverts_AdvertStorage(advert_dict)
            else:
                self.QUERY.insert_adverts_advertstorage(advert_dict)

            self.log.debug("Inserted values to AdvertStorage")

        except:
            # print(adverts['ad_uid'])
            self.log.error("Error when inserting data to AdvertStorage table")
            pass

