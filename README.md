Snapper ReadMe
(Updated)

Installation:

1) Create Virtual environment
    
    ``virtualenv .venv --python=python3.6``
    
2) activate virtualenv
    
    ``source .venv/bin/activate``
    
3) Install requirements.txt dependencies for project

    ``pip install -r requirements.txt``
    
4) Install funpy package
    
    ``pip install git+ssh:git@bitbucket.org:fundmedia/funpy.git``

To Run Project


1) activate virtualenv

    ``source .venv/bin/activate``

2) setup

    ``To run the project (for testing purposes only refer for some useful ids for reference at point (3)``
   ``` 
   i) set few advert enteries in Adverts table to processed=0 so that snapper knows which enteries are
        not processed. 
    
   ii) please note that once when all images are stored, in store directory ensure its emptied before executing 
        for second time. A bug to solve...
   ```
        
2) Run Snapper in terminal after virtualenv is activated

    ``python3 snapper.py``
    
3) Useful database query sets for config testing values
    ```SELECT * FROM Adverts;

       UPDATE Adverts SET processed=0 where ad_id between 218 AND 220;

       UPDATE Adverts SET processed=0 where ad_id=60551;```
